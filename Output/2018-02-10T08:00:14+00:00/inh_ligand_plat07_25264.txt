Parsing the receptor "./5H4I_DOCK.pdbqt"
Enumerating input ligands in "./Ligands/inh_ligand_plat07_25264.pdbqt"
Sorting 1 input ligands in alphabetical order
Seeding a random number generator with 1518249253
Creating an io service pool of 6 worker threads
Calculating a scoring function of 15 atom types
Training a random forest of 500 trees with 42 variables and 4055 samples
Creating grid maps of 0.125 A and running 64 Monte Carlo searches per ligand
   Index             Ligand   nConfs   idock score (kcal/mol)   RF-Score (pKd)
       1   inh_ligand_plat07_25264        9                    -7.54             4.28
