Parsing the receptor "./5H4I_DOCK.pdbqt"
Enumerating input ligands in "./Ligands/inh_ligand_plat07_27244.pdbqt"
Sorting 1 input ligands in alphabetical order
Seeding a random number generator with 1518243024
Creating an io service pool of 6 worker threads
Calculating a scoring function of 15 atom types
Training a random forest of 500 trees with 42 variables and 4055 samples
Creating grid maps of 0.125 A and running 64 Monte Carlo searches per ligand
   Index             Ligand   nConfs   idock score (kcal/mol)   RF-Score (pKd)
       1   inh_ligand_plat07_27244        9                    -7.80             5.23
