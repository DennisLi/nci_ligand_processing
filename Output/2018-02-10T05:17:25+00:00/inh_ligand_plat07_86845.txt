Parsing the receptor "./5H4I_DOCK.pdbqt"
Enumerating input ligands in "./Ligands/inh_ligand_plat07_86845.pdbqt"
Sorting 1 input ligands in alphabetical order
Seeding a random number generator with 1518239546
Creating an io service pool of 6 worker threads
Calculating a scoring function of 15 atom types
Training a random forest of 500 trees with 42 variables and 4055 samples
Creating grid maps of 0.125 A and running 64 Monte Carlo searches per ligand
   Index             Ligand   nConfs   idock score (kcal/mol)   RF-Score (pKd)
       1   inh_ligand_plat07_86845        9                    -9.06             6.34
